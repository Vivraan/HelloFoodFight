tool
extends "res://Scenes/Characters/character.gd"


onready var _ray_cast := $RayCast as RayCast


func _physics_process(_delta: float) -> void:
	if not Engine.editor_hint \
		and _ray_cast.is_colliding():
		_fire()


func _get(property: String):
	return ._get(property)


func _set(property: String, value) -> bool:
	return ._set(property, value)


func _get_property_list() -> Array:
	return ._get_property_list()

