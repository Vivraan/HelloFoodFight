tool
extends KinematicBody

export(String, DIR) var _food_types_path := "res://projectiles/food_types/"
var _projectile_spawn_point: NodePath # export
var _projectile_speed := 50.0 # export
var _can_fire := true
onready var _food_types := HFF.get_filenames(_food_types_path)
onready var _timer := $Timer as Timer


func _ready() -> void:
	randomize()


func _get(property: String):
	match property:
		"projectile/spawn_point":
			return _projectile_spawn_point
		"projectile/speed":
			return _projectile_speed
		_:
			return null


func _set(property: String, value) -> bool:
	match property:
		"projectile/spawn_point":
			_projectile_spawn_point = value as NodePath
			return true
		"projectile/speed":
			_projectile_speed = value as float
			return true
		_:
			return false


func _get_property_list() -> Array:
	return [
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "projectile/spawn_point",
			type = TYPE_NODE_PATH,
			hint = PROPERTY_HINT_NONE,
		},
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "projectile/speed",
			type = TYPE_REAL,
			hint = PROPERTY_HINT_RANGE,
		},
	]


func _fire():
	if _can_fire:
		HFF.launch_random_projectile(
			self, _food_types,
			get_node(_projectile_spawn_point) as Spatial, _projectile_speed)
		_can_fire = false
		_timer.start()


func _on_Timer_timeout() -> void:
	_can_fire = true
