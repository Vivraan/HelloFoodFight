tool
extends "res://Scenes/Characters/character.gd"

export (float, 5, 50) var _movement_speed := 10.0
var _mouse_sensitivity := 1200.0  # export
var _mouse_vertical_bound := 22.5  # export
var _anim_min_blend_speed := 0.125  # export
var _anim_blend_to_run := 0.075  # export
var _anim_blend_to_idle := 0.1  # export
var _motion: Vector3
var _move_blend := 0.0  # idle == 0, run == 1
onready var _anim_tree := $Armature/AnimationTree as AnimationTree
onready var _camera := $Camera as Camera
onready var _armature := $Armature as Skeleton


func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _get(property: String):
	var result = ._get(property)
	if not result:
		match property:
			"animation/blend_to_run":
				return _anim_blend_to_run
			"animation/blend_to_idle":
				return _anim_blend_to_idle
			"animation/min_blend_movement_speed":
				return _anim_min_blend_speed
			"mouse/sensitivity":
				return _mouse_sensitivity
			"mouse/vertical_bound":
				return _mouse_vertical_bound
			_:
				return null
	else:
		return result


func _set(property: String, value) -> bool:
	var result = ._set(property, value)
	if not result:
		match property:
			"animation/blend_to_run":
				_anim_blend_to_run = value as float
				return true
			"animation/blend_to_idle":
				_anim_blend_to_idle = value as float
				return true
			"animation/min_blend_movement_speed":
				_anim_min_blend_speed = value as float
				return true
			"mouse/sensitivity":
				_mouse_sensitivity = value as float
				return true
			"mouse/vertical_bound":
				_mouse_vertical_bound = value as float
				return true
			_:
				return false
	else:
		return result


func _get_property_list() -> Array:
	return ._get_property_list() + [
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "mouse/sensitivity",
			type = TYPE_REAL,
			hint = PROPERTY_HINT_EXP_RANGE,
			hint_string = "300.0,48000.0,300.0",
		},
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "mouse/vertical_bound",
			type = TYPE_REAL,
			hint = PROPERTY_HINT_RANGE,
			hint_string = "0.0, 180.0"
		},
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "animation/min_blend_movement_speed",
			type = TYPE_REAL,
			hint = PROPERTY_HINT_RANGE,
			hint_string = "0.05,0.5,0.005",
		},
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "animation/blend_to_run",
			type = TYPE_REAL,
			hint = PROPERTY_HINT_RANGE,
			hint_string = "0.01,0.5,0.005",
		},
		{
			usage = PROPERTY_USAGE_DEFAULT,
			name = "animation/blend_to_idle",
			type = TYPE_REAL,
			hint = PROPERTY_HINT_RANGE,
			hint_string = "0.01,0.5,0.01",
		},
	]



func _physics_process(_delta: float) -> void:
	if not Engine.editor_hint:
		_move()
		_animate()


func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		var mm_event := event as InputEventMouseMotion
		rotation = _h_camera_rotation(-mm_event.relative.x / _mouse_sensitivity)
		_camera.rotation = _v_camera_rotation(-mm_event.relative.y / _mouse_sensitivity)

	if Input.is_action_just_pressed("fire"):
		_fire()


func _move() -> void:
	var movement_dir := _get_2d_movement_motion()
	var dir_from_cam := Vector3.ZERO
	var cam_xform := _camera.global_transform

	dir_from_cam -= cam_xform.basis.z.normalized() * movement_dir.x
	dir_from_cam += cam_xform.basis.x.normalized() * movement_dir.y
	dir_from_cam.y = 0.0
	_motion = dir_from_cam

# warning-ignore:return_value_discarded
	move_and_slide(_movement_speed * _motion, Vector3.UP)


func _get_2d_movement_motion() -> Vector2:
	var x := Input.get_action_strength("forward") - Input.get_action_strength("back")
	var y := Input.get_action_strength("right") - Input.get_action_strength("left")

	var movement_dir = Vector2(x, y)

	if movement_dir != Vector2.ZERO:
		_face_forward(x, y)
	return movement_dir.normalized()


func _face_forward(x: float, y: float):
	_armature.rotation.y = atan2(x, y) - deg2rad(90.0)


func _h_camera_rotation(cam_rotation: float) -> Vector3:
	return rotation + Vector3(0.0, cam_rotation, 0.0)


func _v_camera_rotation(cam_rotation: float) -> Vector3:
	var rot := _camera.rotation + Vector3(cam_rotation, 0.0, 0.0)
	var bound := deg2rad(_mouse_vertical_bound)
	rot.x = clamp(rot.x, -bound, bound)
	return rot


func _animate() -> void:
	if _movement_speed * _motion.length() > _anim_min_blend_speed:
		_move_blend += _anim_blend_to_run
	else:
		_move_blend -= _anim_blend_to_idle

	_move_blend = clamp(_move_blend, 0.0, 1.0)
	_anim_tree["parameters/Move/blend_amount"] = _move_blend
