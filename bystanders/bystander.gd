extends RigidBody



func _on_VisibilityNotifier_camera_exited(_camera: Camera) -> void:
	queue_free()


func hurt() -> void:
	($AnimationPlayer as AnimationPlayer).play("Die")
