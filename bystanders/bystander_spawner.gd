extends Position3D


export(String, DIR) var _bystander_types_path := "res://bystanders/bystander_types/"
export var _min_waiting_time := 1
export var _max_waiting_time := 5
export var _speed := 10.0
onready var _bystander_types := HFF.get_filenames(_bystander_types_path)
onready var _timer := $Timer as Timer


func _init() -> void:
	assert(
		_max_waiting_time > _min_waiting_time
		and _max_waiting_time > 0
		and _min_waiting_time > 0,
		"ERROR: _max_waiting_time (={0}) must be larger than _min_waiting_time (={1}) and both must be positive.".format([_max_waiting_time, _min_waiting_time]))


func _ready() -> void:
	randomize()
	_set_timer()


func _on_Timer_timeout() -> void:
	_set_timer()
	HFF.launch_random_projectile(self, _bystander_types, self, _speed)


func _set_timer() -> void:
	_timer.wait_time = randi() % _max_waiting_time + _min_waiting_time
	_timer.start()
