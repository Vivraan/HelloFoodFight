extends CanvasLayer


const _window_mode_normal := "Windowed"
const _window_mode_fullscreen := "Fullscreen"
onready var _pause_popup := $Popup as Popup
onready var _windowing_mode_button := $Popup/ColorRect/VBoxContainer/WindowingMode/ToggleWindowingMode as Button


func _input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().paused = not get_tree().paused
		if get_tree().paused:
			_pause_popup.show()
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			_pause_popup.hide()
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _on_ToggleWindowingMode_pressed() -> void:
	OS.window_fullscreen = not OS.window_fullscreen
	_windowing_mode_button.text = \
		_window_mode_normal if OS.window_fullscreen else _window_mode_fullscreen


func _on_Customize_pressed() -> void:
	pass # Replace with function body.


func _on_DoQuit_pressed() -> void:
	get_tree().quit(0)
