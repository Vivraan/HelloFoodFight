class_name HFF
extends Reference


static func get_filenames(path: String) -> PoolStringArray:
	var files := PoolStringArray()

	var dir := Directory.new()
	var err := dir.open(path)
	if err == OK:
		if dir.list_dir_begin() == OK:
			while true:
				var file := dir.get_next() as String
				if not file or file.empty():
					break
				elif not file.begins_with("."):
					files.append(path + file)
			dir.list_dir_end()
	else:
		printerr("Error while trying to read directory: " + str(err))

	return files


static func launch_random_projectile(
	parent: Spatial, projectile_scenes: PoolStringArray,
	spawn_point: Spatial, speed: float) \
	-> void:
	var random_scene: String = projectile_scenes[randi() % projectile_scenes.size()]
	var projectile := load(random_scene).instance() as RigidBody
	parent.add_child(projectile)
	projectile.set_as_toplevel(true)
	projectile.global_transform = spawn_point.global_transform
	var parent_forward := spawn_point.global_transform.basis.z.normalized()
	projectile.linear_velocity = speed * parent_forward
