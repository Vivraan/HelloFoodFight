extends RigidBody


func _ready() -> void:
	($Timer as Timer).start()


func _on_Timer_timeout() -> void:
	queue_free()


func _on_Projectile_body_shape_entered(_body_id: int, body: Node, _body_shape: int, _local_shape: int) -> void:
	if body.has_method("hurt"):
		body.call("hurt")
		queue_free()
